import csv
import datetime
import json

from scripts import constants
from dateutil.parser import parse

DATE_FORMAT = "%Y%m%d"
TIME_FORMAT = "%H:%M"


#
# http://stockcharts.com/school/doku.php?id=chart_school:chart_analysis:heikin_ashi
#
# https://quantiacs.com/Blog/Intro-to-Algorithmic-Trading-with-Heikin-Ashi.aspx
#
class HeikinAsi:
    def __init__(self, name, outFp):
        file = '../data/%s_20160701_20160927_10m.dump' % (name.upper())
        self.rawJson = json.load(open(file, 'r'))
        self.name = name
        self.populateDateWiseData()
        self.populateHeikinAsiData()
        self.outFP = outFp

    def populateDateWiseData(self):
        self.dateWiseData = {}
        for ix in range(0, len(self.rawJson)):
            obj = self.rawJson[ix]
            date_str = obj["date"]
            d = parse(date_str)
            date_key = d.strftime(DATE_FORMAT)
            if date_key not in self.dateWiseData.keys():
                self.dateWiseData[date_key] = []
            self.dateWiseData[date_key].append([obj, d])
        for key in self.dateWiseData.keys():
            self.dateWiseData[key] = sorted(self.dateWiseData[key], key=lambda obj: obj[1])

    def populateHeikinAsiData(self):
        self.heikinAsiData = {}
        for key, value in self.dateWiseData.items():
            heikinAsiDataForADay = []
            for ix in range(0, len(value)):
                dict = {}
                data = value[ix][0]  # current data
                if (ix == 0):
                    # calc on first run
                    ha_close = (data["open"] + data["close"] + data["high"] + data["low"]) / 4
                    ha_open = (data["open"] + data["close"]) / 2
                    ha_high = data["high"]
                    ha_low = data["low"]
                else:
                    # calc on second run onwards
                    prvHAData = heikinAsiDataForADay[ix - 1]
                    ha_close = (data["open"] + data["close"] + data["high"] + data["low"]) / 4
                    ha_open = (prvHAData["HAOpen"] + prvHAData["HAClose"]) / 2
                    ha_high = max(data["high"], ha_open, ha_close)
                    ha_low = min(data["low"], ha_open, ha_close)

                dict["HAClose"] = ha_close
                dict["HAOpen"] = ha_open
                dict["HAHigh"] = ha_high
                dict["HALow"] = ha_low
                dict["actual"] = data
                heikinAsiDataForADay.append(dict)
            self.heikinAsiData[key] = heikinAsiDataForADay
        print " HeikinAsiData calculated for: " + self.name

    def heiKinAsiTrade(self, date):
        if date not in self.heikinAsiData.keys():
            return None

        dataToday = self.heikinAsiData[date]

        for ix in range(1, len(dataToday)):
            dictCur = dataToday[ix]
            dictPrv = dataToday[ix - 1]
            dateObj = parse(dictCur["actual"]["date"])

            # Check only till 10:30
            if dateObj.hour <= 10 and dateObj.minute <= 30:
                # latest HA candle is bearish, HA_Close < HA_Open
                long1 = dictCur["HAClose"] < dictCur["HAOpen"]

                # current candle body is longer than previous candle body
                long2 = abs(dictCur["HAClose"] - dictCur["HAOpen"]) > abs(dictPrv["HAClose"] - dictPrv["HAOpen"])

                # previous candle was bearish
                long3 = dictPrv["HAClose"] < dictPrv["HAOpen"]

                # latest candle has no upper wick HA_Open == HA_High
                long4 = dictCur["HAOpen"] == dictCur["HAHigh"]

                long = long1 and long2 and long3 and long4

                # Pick this stock if above all 4 conditions met
                if long is True:
                    print "Buy " + self.name
                    dictAfter30min = dataToday[ix + 3]
                    dictAfter40min = dataToday[ix + 4]
                    dictAfter50min = dataToday[ix + 5]
                    dictAfter60min = dataToday[ix + 6]
                    self.outFP.writerow([self.name,
                                         datetime.datetime.strftime(dateObj, DATE_FORMAT),
                                         datetime.datetime.strftime(dateObj, TIME_FORMAT),
                                         dictCur["actual"]["close"],
                                         dictAfter30min["actual"]["close"],
                                         dictAfter40min["actual"]["close"],
                                         dictAfter50min["actual"]["close"],
                                         dictAfter60min["actual"]["close"],
                                         (dictAfter30min["actual"]["close"] - dictCur["actual"]["close"]) / 100,
                                         (dictAfter40min["actual"]["close"] - dictCur["actual"]["close"]) / 100,
                                         (dictAfter50min["actual"]["close"] - dictCur["actual"]["close"]) / 100,
                                         (dictAfter60min["actual"]["close"] - dictCur["actual"]["close"]) / 100])


if __name__ == "__main__":
    print "Starting Heikin Asi Implementation"
    constants.initLogging()

d = datetime.datetime.strptime("20160701", DATE_FORMAT)
o = csv.writer(open('../data/predictHeikinAsi.csv', 'w'), delimiter=",")
o.writerow(
    ["SYMBOL", "DATE", "TRADE_TIME", "LTP", "LTP_AFTER_30_MIN", "LTP_AFTER_40_MIN", "LTP_AFTER_50_MIN",
     "LTP_AFTER_60_MIN", "%_AFTER_30_MIN", "%_AFTER_40_MIN", "%_AFTER_50_MIN",
     "%_AFTER_60_MIN"])

for ix in range(0, 20):
    newD = d + datetime.timedelta(days=ix)
    f = csv.reader(open('../data/top50.csv'), delimiter=",")
    for row in f:
        name = row[0]
        s = HeikinAsi(name=name, outFp=o)
        s.heiKinAsiTrade(newD.strftime(DATE_FORMAT))

print "_____________ Execution completed _______________"
