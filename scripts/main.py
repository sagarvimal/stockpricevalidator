import csv
import logging

import sys
from kiteconnect import KiteConnect
import  constants

import json
class KiteHelper:

    def test(self):
        self.kite = KiteConnect(api_key=constants.API_KEY)
        print self.kite.login_url()
        data = self.kite.request_access_token("g4fbhuommuou3rxov1djg07k8cm3rmth", secret="lbj7h9t8p5dm9uzspgwkxqvnzrvin0n4")
        self.kite.set_access_token(data["access_token"])
        self.kite.instruments()

    def findInstrumentId(self, exchange, tradingSymbol):
        if exchange == None:
            logging.error("no exchange name passed")
        tradingSymbol = tradingSymbol.strip().upper()
        instrument = json.load(open('data/instrument.txt', 'r'))
        obj = None
        success = False
        for ix in range(len(instrument)):
            obj = instrument[ix]
            if obj['exchange'] == exchange:
                if obj['tradingsymbol'] == tradingSymbol:
                    success = True
                    break
        if success:
            logging.debug(str(obj))
            return obj['instrument_token']
        else:
            logging.error("not found")
            return None


    def findHistoricalData(self, exchange, tradeSymbol, fromDate, toDate, interval):
        instrumentId = self.findInstrumentId(exchange=exchange, tradingSymbol=tradeSymbol)
        if instrumentId == None:
            logging.error("instrument id not found")
            sys.exit(1)
        data = self.kite.historical(instrument_token=instrumentId, from_date=fromDate, to_date=toDate, interval= interval)


    def findAllData(self):
        f = csv.reader(open('/Users/sjain/Desktop/top50.csv','r'), delimiter=",")
        for row in f:
            id = row[0]
            instrument_id = self.findInstrumentId('BSE',id)
            data = self.kite.historical(instrument_token=instrument_id, from_date='2016-07-01', to_date='2016-09-27',
                                        interval='10minute')
            json.dump(data,open('data/'+id+'_20160701_20160927_10m.dump','w'))


if __name__ == "__main__":
    constants.initLogging()
    obj = KiteHelper()
    obj.findInstrumentId('NSE', 'AARTIIND-BL')
