import csv
import json
import datetime
import logging

import sys
import traceback

import sklearn

import constants

from dateutil.parser import parse
from sklearn.model_selection import cross_val_score


DATE_FORMAT = "%Y%m%d"


inputArr = []
outputArr = []
from sklearn import svm
from sklearn import linear_model
from sklearn import tree
class Stock:
    rawJson = None
    dateWiseData = {}
    confidence = 0.6
    analyzeTime = 21
    totalTime = 120
    th = 0.02

    uth = 0.2
    lth = -2



    def __init__(self, name, outFp):
        file = '../data/%s_20160701_20160927_3m.dump' % (name.upper())
        self.rawJson = json.load(open(file, 'r'))
        self.name = name
        self.populateDateWiseData()
        self.outFP= outFp

    def populateDateWiseData(self):
        self.dateWiseData = {}
        for ix in range(0, len(self.rawJson)):
            obj = self.rawJson[ix]
            date_str = obj["date"]
            d = parse(date_str)
            date_key = d.strftime(DATE_FORMAT)
            if date_key not in self.dateWiseData.keys():
                self.dateWiseData[date_key] = []
            self.dateWiseData[date_key].append([obj, d])
        for key in self.dateWiseData.keys():
            self.dateWiseData[key] = sorted(self.dateWiseData[key], key=lambda obj: obj[1])


    def getEODPrice(self, date):
        if date not in self.dateWiseData.keys():
            logging.debug("%s no data found" % (date))
            return None
        else:
            arr = self.dateWiseData[date]
            return self.dateWiseData[date][len(arr) - 1]

    def movingAverage(self, startDate, lookback):
        prices = []
        d = datetime.datetime.strptime(startDate, DATE_FORMAT)
        ix = 0
        jx = 0
        while ix < lookback and jx < (3 * lookback):
            curD = d - datetime.timedelta(days=jx)
            key = curD.strftime(DATE_FORMAT)
            obj = self.getEODPrice(key)
            if obj:
                prices.append(obj[0]['close'])
                ix += 1
            jx += 1
        if jx >= (3 * lookback):
            logging.debug(
                "%s Was not able to find data for %d lookback days. "
                "Breaking out after %d lookback days" % (startDate, lookback, jx))
        return round(float(sum(prices)) / len(prices),2), len(prices)


    def predictTrend(self, date):
        if date not in self.dateWiseData.keys():
            return None

        dataToday = self.dateWiseData[date]
        totalAnalyzeTime = self.analyzeTime #time to analyze stock
        startTime = dataToday[0][1]
        dataPoints = []

        for ix in range(1, len(dataToday)):
            curObj = dataToday[ix]
            if curObj[1] - startTime > datetime.timedelta(minutes=totalAnalyzeTime):
                break
            dataPoints.append([dataToday[ix - 1][0]['close'], curObj[0]['close']])
        # binary scoring
        logging.debug(dataPoints)
        changes = map(lambda x: x[1]/float(x[0]), dataPoints)
        logging.debug(changes)
        logging.debug(filter(lambda x: x > 1, changes))
        totalPositive = len(filter(lambda x: x > 1, changes))
        buyStock = totalPositive > (len(dataPoints) - 1) * self.confidence
        buyPrice = dataPoints[len(dataPoints) - 1][1]
        if buyStock:
            perChange = self.checkProfitability(date, totalAnalyzeTime, buyPrice,
                                              totalPositive / float(len(dataPoints) - 1))
            startPer = (buyPrice / float(dataToday[0][0]['close']) - 1) * 100
            self.generateFeatureVector(changes, startPer, perChange>self.uth)
        else:
            logging.debug("no buy on %s", date)
    def generateFeatureVector(self, changes, startPer, predict):
        global  inputArr, outputArr
        arr = []
        for obj in changes:
            arr.append(obj)

        arr.append(startPer)
        # adding actual output
        # arr.append(1 if predict == True else 0)
        inputArr.append(arr)

        # outputArr.append(predict)
        outputArr.append(1 if predict==True else 0)



    def findMaxReachTime(self, date):
        if date not in self.dateWiseData.keys():
            return None
        dataToday = self.dateWiseData[date]
        startTime = dataToday[0][1]

        max  = -1
        maxTime = -1
        for ix in range(1, len(dataToday)):
            curObj = dataToday[ix]
            if curObj[0]['close']> max:
                max = curObj[0]['close']
                maxTime = float((curObj[1]-startTime).seconds)/60
        return [date, self.name, max/dataToday[0][0]['close']-1, maxTime]


    def checkProfitability(self, date, startOffset, buyPrice, confidence):
        if date not in self.dateWiseData.keys():
            return None
        dataToday = self.dateWiseData[date]

        startTime = dataToday[0][1] + datetime.timedelta(minutes=startOffset)

        dataPoints = []
        finalPrice = None
        timeUp = False

        for ix in range(1, len(dataToday)):
            curObj = dataToday[ix]
            if curObj[1] < startTime:
                continue
            closeVal = float(curObj[0]['close'])
            finalPrice = closeVal
            minutesTillNow = float((curObj[1] - startTime).seconds) / 60
            perChange = (closeVal / buyPrice - 1) * 100

            # dataPoints.append([closeVal, closeVal - buyPrice, (closeVal / buyPrice - 1) * 100, (curObj[1]-startTime).seconds/3600])
            dataPoints.append([perChange, minutesTillNow])

            if perChange > self.uth or perChange < self.lth:
                break

            if minutesTillNow > self.totalTime:
                # logging.info("no hit in %d time - %s" % (self.totalTime, self.name))
                timeUp = True
                break

            # binary scoring

        logging.info("%s upside %f %f %s %f", date, dataPoints[len(dataPoints)-1][0],
                     dataPoints[len(dataPoints)-1][1], self.name, confidence)
        self.outFP.writerow(map(str, [date, dataPoints[len(dataPoints)-1][0],
                                      dataPoints[len(dataPoints) - 1][0], self.name, confidence]))
        return dataPoints[len(dataPoints)-1][0]

        # dataPoints = sorted(dataPoints, key=lambda x:x[0], reverse=True)

        # logging.info("%s max profit on %f, min low %f, on %s confidence %f %f", date, dataPoints[0][0],
        #              dataPoints[len(dataPoints)-1][0], self.name, confidence, dataPoints[0][1])
        # if dataPoints[0][0] > self.th:
        #     self.outFP.writerow(map(str,[date, dataPoints[0][0],
        #                  dataPoints[len(dataPoints)-1][0], self.name, confidence, dataPoints[0][1]]))
        # else:
        #     logging.info("loss incurred on %s of %f"%(self.name, dataPoints[len(dataPoints)-1][0]))
        #     self.outFP.writerow(map(str, [date, dataPoints[len(dataPoints)-1][0],
        #                                   dataPoints[len(dataPoints) - 1][0], self.name, confidence, dataPoints[0][1]]))


def computePrecissionRecall(clf, featureArr, outputArr):
    match = 0
    p = 0
    r = 0
    tp = 0
    tp2 = 0
    for ix in range(len(featureArr)):
        out = clf.predict([featureArr[ix]])
        if outputArr[ix] == out[0]:
            match += 1
            if outputArr[ix] == 1:
                r += 1

        if outputArr[ix] == 1:
            tp += 1

        if out[0] == 1:
            tp2 += 1

    return [float(r)/tp2, float(r)/tp]

if __name__ == "__main__":
    constants.initLogging()

    clf = linear_model.LogisticRegression()
    d = datetime.datetime.strptime("20160704", DATE_FORMAT)
    o = csv.writer(open('../data/maxReach.csv', 'w'), delimiter = ",")

    maxEntry = 50
    for ix in range(0, 5):
        newD = d + datetime.timedelta(days=ix)
        f = csv.reader(open('../data/top50.csv'), delimiter=",")
        try:
            for row in f:
                name = row[0]
                s = Stock(name=name, outFp=o)
                if s.rawJson == None:
                    break
                # obj = s.findMaxReachTime(newD.strftime(DATE_FORMAT))
                # if obj == None:
                #     continue
                # print obj
                # o.writerow(obj)
                s.predictTrend(newD.strftime(DATE_FORMAT))
                if len(inputArr)==maxEntry:
                    break
            if len(inputArr) == maxEntry:
                break
        except Exception as e:
            traceback.print_exc()

    print cross_val_score(clf,inputArr, outputArr, cv=3)
    clf.fit(inputArr, outputArr)
    print clf.coef_

    computePrecissionRecall(clf, inputArr, outputArr)