# Initialise.
from kiteconnect import WebSocket
import constants
import main


instrumentId = main.findInstrumentId('NSE','INDIGO')
kws = WebSocket(constants.API_KEY, "aafd32f70c57a6139d417a63c3043470", "AS1942")

# Callback for tick reception.
def on_tick(tick, ws):
    print tick

# Callback for successful connection.
def on_connect(ws):
    # Subscribe to a list of instrument_tokens (RELIANCE and ACC here).
    ws.subscribe([])

    # Set RELIANCE to tick in `full` mode.
    ws.set_mode(ws.MODE_LTP, [738561])

# Assign the callbacks.
kws.on_tick = on_tick
kws.on_connect = on_connect

# Infinite loop on the main thread. Nothing after this will run.
# You have to use the pre-defined callbacks to manage subscriptions.
kws.connect()