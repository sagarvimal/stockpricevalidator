import java.util.Date;

public class Stock implements Comparable<Stock> {

	// "Symbol","Series","Date","Prev Close","Open Price","High Price","Low
	// Price","Last Price","Close Price","Average Price","Total Traded
	// Quantity","Turnover in Lacs","No. of Trades","Deliverable Qty","% Dly Qt
	// to Traded Qty"

	private String symbol;

	private String series;

	private Date date;

	private Double prvClose;

	private Double openPrice;

	private Double highPrice;

	private Double lowPrice;

	private Double closePrice;

	private Double totalQuantity;

	private Double turnOver; // in lakhs

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getSeries() {
		return series;
	}

	public void setSeries(String series) {
		this.series = series;
	}

	public Double getPrvClose() {
		return prvClose;
	}

	public void setPrvClose(Double prvClose) {
		this.prvClose = prvClose;
	}

	public Double getOpenPrice() {
		return openPrice;
	}

	public void setOpenPrice(Double openPrice) {
		this.openPrice = openPrice;
	}

	public Double getHighPrice() {
		return highPrice;
	}

	public void setHighPrice(Double highPrice) {
		this.highPrice = highPrice;
	}

	public Double getLowPrice() {
		return lowPrice;
	}

	public void setLowPrice(Double lowPrice) {
		this.lowPrice = lowPrice;
	}

	public Double getClosePrice() {
		return closePrice;
	}

	public void setClosePrice(Double closePrice) {
		this.closePrice = closePrice;
	}

	public Double getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(Double totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public Double getTurnOver() {
		return turnOver;
	}

	public void setTurnOver(Double turnOver) {
		this.turnOver = turnOver;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int compareTo(Stock compareStock) {
		int result = 0;
		if (this.getDate().after(compareStock.getDate())) {
			result = 1;
		} else if (this.getDate().before(compareStock.getDate())) {
			result = -1;
		}
		return result;
	}
}
