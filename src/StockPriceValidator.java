import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

public class StockPriceValidator {

	private static final String URL_START = "https://www.nseindia.com/products/dynaContent/common/productsSymbolMapping.jsp?symbol=";
	private static final String URL_END = "&segmentLink=3&symbolCount=1&series=ALL&dateRange=12month&fromDate=&toDate=&dataType=PRICEVOLUMEDELIVERABLE";
	private static final String DATE_FORMAT_1 = "dd-MMM-yyyy";
	private static final String DATE_FORMAT_2 = "ddMMyyyy";
	private static final String INPUT_FILE_PATH = "data/Stock_Prediction_Sheet1.csv";
	private static final String OUTPUT_FILE_PATH = "data/Stock_Prediction_output.csv";

	public static void main(String[] args) {

		ArrayList<Input> inputs = getSymbols();
		if (inputs != null && inputs.size() > 0) {
			ArrayList<Result> results = new ArrayList<>();
			int count = 0;
			for (Input input : inputs) {
				System.out.println("Processing for: " + input.getSymbol());
				Result result = processData(input);
				if (result != null) {
					System.out.println(
							"Target reached !! " + result.getSymbol() + " Date: " + result.getTargetReachDate());
					if (result.getIsHit().equals("YES")) {
						count++;
					}
					results.add(result);
				}
			}
			updateResult(results);
			System.out.println("Execution complete");
			System.out.println("Total hits: " + count);
			System.out.println("Total scripts: " + inputs.size());
		}
	}

	/**
	 * Get input from CSV
	 * 
	 * @return
	 */
	private static ArrayList<Input> getSymbols() {
		CSVReader reader = null;
		ArrayList<Input> inputs = new ArrayList<>();
		Input input = null;
		try {
			reader = new CSVReader(new FileReader(Paths.get(".").toAbsolutePath() + "/" + INPUT_FILE_PATH));
			List<String[]> allRows;
			allRows = reader.readAll();
			// i starts from 1 to exclude header of csv
			for (int i = 1; i < allRows.size(); i++) {
				try {
					input = new Input();
					input.setSymbol(allRows.get(i)[1]);
					input.setStartDate(formatDate(allRows.get(i)[0], DATE_FORMAT_2));
					input.setStartPrice(Double.parseDouble(removeQuotes(allRows.get(i)[2])));
					input.setTargetPrice(Double.parseDouble(removeQuotes(allRows.get(i)[3])));
					input.setGivenDays(Integer.parseInt(allRows.get(i)[4]));

					inputs.add(input);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return inputs;
	}

	/**
	 * Write result to output csv
	 * 
	 * @param result
	 */
	private static void updateResult(ArrayList<Result> results) {
		CSVWriter writer = null;
		try {
			writer = new CSVWriter(new FileWriter(OUTPUT_FILE_PATH, true));

			String[] header = { "SYMBOL", "SERIES", "IS_TARGET_HIT", "START_DATE", "START_PRICE", "TARGET_PRICE",
					"TARGET_REACH_PRICE", "TARGET_REACH_DATE", "NUM_OF_DAYS_GIVEN", "NUM_OF_DAYS_TAKEN", "PRICE_DIFF_%",
					"CURRENT_DATE", "CURRENT_PRICE", "PROPOSED_DATE", "PRICE_AT_PROPOSED_DATE",
					"PRICE_DIFF_PROPOSED_DATE_%", "COMMENT" };
			writer.writeNext(header);

			String[] record = null;
			if (results != null && results.size() > 0) {
				for (Result result : results) {
					record = new String[17];
					record[0] = result.getSymbol();
					record[1] = result.getSeries();
					record[2] = result.getIsHit();
					record[3] = result.getStartDate();
					record[4] = getFormatedDouble(result.getStartingPrice());
					record[5] = getFormatedDouble(result.getTarget());
					record[6] = getFormatedDouble(result.getTargetReachPrice());
					record[7] = String.valueOf(result.getTargetReachDate());
					record[8] = String.valueOf(result.getGivenDays());
					record[9] = String.valueOf(result.getNumberOfDays());
					record[10] = getFormatedDouble(result.getChange());
					record[11] = String.valueOf(result.getCurrentDate());
					record[12] = getFormatedDouble(result.getCurrentPrice());
					record[13] = result.getDateAfterGivenDays();
					record[14] = getFormatedDouble(result.getPriceAfterGivenDays());
					record[15] = getFormatedDouble(result.getPriceChangeAfterGivenDays());
					record[16] = result.getComment();
					writer.writeNext(record);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (writer != null) {
					writer.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Get 12 month price list of stock from NSE
	 * 
	 * @param symbol
	 * @return
	 */
	private static ArrayList<Stock> getStockPrices(String symbol) {
		ArrayList<Stock> data = null;
		Stock stock = null;
		try {
			Document doc = Jsoup.connect(URL_START + symbol + URL_END).get();

			if (doc != null) {
				Elements info = doc.select("div#csvContentDiv");
				if (info != null && info.size() > 0) {
					String[] rows = info.text().split(":");
					String[] column = null;
					if (rows != null) {
						data = new ArrayList<>();
						// i starts from 1 to exclude header of csv
						for (int i = 1; i < rows.length; i++) {
							column = rows[i].split(",");
							if (column != null) {
								stock = new Stock();
								stock.setSymbol(removeQuotes(column[0]));
								stock.setSeries(removeQuotes(column[1]));
								stock.setDate(formatDate(removeQuotes(column[2]), DATE_FORMAT_1));
								stock.setPrvClose(Double.parseDouble(removeQuotes(column[3])));
								stock.setOpenPrice(Double.parseDouble(removeQuotes(column[4])));
								stock.setHighPrice(Double.parseDouble(removeQuotes(column[5])));
								stock.setLowPrice(Double.parseDouble(removeQuotes(column[6])));
								stock.setClosePrice(Double.parseDouble(removeQuotes(column[8])));
								stock.setTotalQuantity(Double.parseDouble(removeQuotes(column[10])));
								stock.setTurnOver(Double.parseDouble(removeQuotes(column[11])));
								data.add(stock);
							}
						}
					}
				} else {
					System.out.println("Could not fetch data. Check script symbol. Current symbol: " + symbol);
				}
			} else {
				System.out.println("Doc is null");
			}

		} catch (Exception e) {
			System.out.println("Exception ...!!!");
			e.printStackTrace();
		}

		// sort list
		if (data != null && data.size() > 0) {
			Collections.sort(data);
		}

		return data;
	}

	private static String removeQuotes(String str) {
		if (str != null) {
			str = str.replaceAll("\"", "").trim();
		}
		return str;
	}

	private static Date formatDate(String dateStr, String dateFormat) {
		DateFormat df = new SimpleDateFormat(dateFormat);
		Date date = null;
		try {
			date = df.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	private static String getPresentableDate(Date date, String dateFormat) {
		DateFormat df = new SimpleDateFormat(dateFormat);
		String dateStr = null;
		try {
			dateStr = df.format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dateStr;
	}

	/**
	 * Process the stock with target price
	 * 
	 * @param symbol
	 * @param input
	 */
	private static Result processData(Input input) {
		ArrayList<Stock> stocks = getStockPrices(input.getSymbol());
		Result result = null;
		boolean isHit = false;
		if (stocks != null && stocks.size() > 0) {
			int numDays = 0;
			result = new Result();
			result.setSymbol(input.getSymbol());
			result.setCurrentDate(getPresentableDate(stocks.get(stocks.size() - 1).getDate(), DATE_FORMAT_1));
			result.setCurrentPrice(stocks.get(stocks.size() - 1).getClosePrice());
			result.setGivenDays(input.getGivenDays());
			result.setStartDate(getPresentableDate(input.getStartDate(), DATE_FORMAT_1));
			result.setStartingPrice(input.getStartPrice());
			result.setTarget(input.getTargetPrice());
			result.setSeries(stocks.get(stocks.size() - 1).getSeries());
			for (Stock stock : stocks) {
				numDays = Days.daysBetween(new DateTime(input.getStartDate()).withTimeAtStartOfDay(),
						new DateTime(stock.getDate()).withTimeAtStartOfDay()).getDays();
				if (stock.getDate().after(input.getStartDate()) && input.getTargetPrice() <= stock.getHighPrice()
						&& input.getTargetPrice() >= stock.getLowPrice() && numDays <= input.getGivenDays()) {
					result.setNumberOfDays(numDays);
					result.setTargetReachDate(getPresentableDate(stock.getDate(), DATE_FORMAT_1));
					result.setTargetReachPrice(stock.getClosePrice());
					result.setChange(((stock.getClosePrice() - input.getStartPrice()) / input.getStartPrice()) * 100);
					isHit = true;

					break;
				}
			}

			// if target is hit
			if (isHit) {
				result.setIsHit("YES");
			} else {
				result.setIsHit("NO");
				result.setNumberOfDays(0);
				result.setTargetReachDate("-");
				result.setTargetReachPrice(0.0);
				result.setChange(0.0);
			}

			// Update price after given number of days
			updatePriceAfterGivenDays(stocks, input, result);

			// add comments if any
			if (input.getStartPrice() >= input.getTargetPrice()) {
				result.setComment("Start price is greater than target price");
			}
		}
		return result;
	}

	/**
	 * Updates the price after given number of days
	 * 
	 * @param stocks
	 * @param input
	 * @param result
	 */
	private static void updatePriceAfterGivenDays(ArrayList<Stock> stocks, Input input, Result result) {
		DateTime proposedDateTime = new DateTime(input.getStartDate());
		proposedDateTime = proposedDateTime.plusDays(input.getGivenDays());

		// if it is future date
		if (proposedDateTime.isAfter(new DateTime(new Date()))) {
			result.setComment("Future date. " + Days.daysBetween(new DateTime(new Date()).withTimeAtStartOfDay(),
					proposedDateTime.withTimeAtStartOfDay()).getDays() + " more days left");
			result.setDateAfterGivenDays("-");
			result.setPriceAfterGivenDays(0.0);
			result.setPriceChangeAfterGivenDays(0.0);
			return;
		}

		DateTime dateTime1 = null;
		for (Stock stock : stocks) {
			dateTime1 = new DateTime(stock.getDate());
			if (dateTime1.isAfter(proposedDateTime)) {
				result.setDateAfterGivenDays(getPresentableDate(stock.getDate(), DATE_FORMAT_1));
				result.setPriceAfterGivenDays(stock.getClosePrice());
				result.setPriceChangeAfterGivenDays(
						((stock.getClosePrice() - input.getStartPrice()) / input.getStartPrice()) * 100);
				break;
			}
		}
	}

	private static String getFormatedDouble(Double value) {
		DecimalFormat df = new DecimalFormat("#.##");
		return df.format(value);
	}
}
