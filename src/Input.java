import java.util.Date;

public class Input {

	private String symbol;

	private int givenDays;

	private Date startDate;

	private Double targetPrice;

	private Double startPrice;

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public int getGivenDays() {
		return givenDays;
	}

	public void setGivenDays(int givenDays) {
		this.givenDays = givenDays;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Double getTargetPrice() {
		return targetPrice;
	}

	public void setTargetPrice(Double targetPrice) {
		this.targetPrice = targetPrice;
	}

	public Double getStartPrice() {
		return startPrice;
	}

	public void setStartPrice(Double startPrice) {
		this.startPrice = startPrice;
	}
}
