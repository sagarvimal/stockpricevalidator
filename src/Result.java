
public class Result {

	private String currentDate;
	
	private String startDate;
	
	private String targetReachDate;
	
	private Double currentPrice;
	
	/**
	 * Closing price of the days when target is reached
	 */
	private Double targetReachPrice;
	
	private Double target;
	
	private Double startingPrice;
	
	private int numberOfDays;
	
	/**
	 * Change in price when target is hit
	 */
	private Double change;
	
	private String symbol;
	
	private int givenDays;
	
	/**
	 * IS the price reached target price in given days
	 */
	private String isHit;
	
	/**
	 * Any comment
	 */
	private String comment;
	
	private Double priceAfterGivenDays;
	
	private Double priceChangeAfterGivenDays;
	
	/**
	 * Series at current date
	 */
	private String series;
	
	/**
	 * First date after given days
	 */
	private String dateAfterGivenDays;

	public String getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(String currentDate) {
		this.currentDate = currentDate;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getTargetReachDate() {
		return targetReachDate;
	}

	public void setTargetReachDate(String targetReachDate) {
		this.targetReachDate = targetReachDate;
	}

	public Double getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(Double currentPrice) {
		this.currentPrice = currentPrice;
	}

	public Double getTargetReachPrice() {
		return targetReachPrice;
	}

	public void setTargetReachPrice(Double targetReachPrice) {
		this.targetReachPrice = targetReachPrice;
	}

	public Double getStartingPrice() {
		return startingPrice;
	}

	public void setStartingPrice(Double startingPrice) {
		this.startingPrice = startingPrice;
	}

	public int getNumberOfDays() {
		return numberOfDays;
	}

	public void setNumberOfDays(int numberOfDays) {
		this.numberOfDays = numberOfDays;
	}

	public Double getChange() {
		return change;
	}

	public void setChange(Double change) {
		this.change = change;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public int getGivenDays() {
		return givenDays;
	}

	public void setGivenDays(int givenDays) {
		this.givenDays = givenDays;
	}

	public Double getTarget() {
		return target;
	}

	public void setTarget(Double target) {
		this.target = target;
	}

	public String getIsHit() {
		return isHit;
	}

	public void setIsHit(String isHit) {
		this.isHit = isHit;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Double getPriceAfterGivenDays() {
		return priceAfterGivenDays;
	}

	public void setPriceAfterGivenDays(Double priceAfterGivenDays) {
		this.priceAfterGivenDays = priceAfterGivenDays;
	}

	public Double getPriceChangeAfterGivenDays() {
		return priceChangeAfterGivenDays;
	}

	public void setPriceChangeAfterGivenDays(Double priceChangeAfterGivenDays) {
		this.priceChangeAfterGivenDays = priceChangeAfterGivenDays;
	}

	public String getSeries() {
		return series;
	}

	public void setSeries(String series) {
		this.series = series;
	}

	public String getDateAfterGivenDays() {
		return dateAfterGivenDays;
	}

	public void setDateAfterGivenDays(String dateAfterGivenDays) {
		this.dateAfterGivenDays = dateAfterGivenDays;
	}
}
